<?php
    include 'koneksi.php';
    $db = new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pegawai - Update Data</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Data Pegawai</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Pegawai</a>
                <a class="nav-item nav-link active" href="pegawai.php">Pegawai<span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link" href="status.php">Status</a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Update Data Pegawai</h4>
<?php foreach($db->editdata($_GET['kode']) as $mem) : ?>
<form action="proses.php?aksi=update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="kode">Kode</label>
            <input type="hidden" id="kode" name="kode" value="<?= $mem['kode'] ?>"><br>
            <?= $mem['kode'] ?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Pegawai</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required value="<?= $mem['nama'] ?>">
        </div>
        <div class="form-group">
            <label for="jenkel">Jenis Kelamin</label>
            <input type="text" placeholder="Masukkan Jenis Kelamin" id="jenkel" name="jenkel" class="form-control" required value="<?= $mem['telepon'] ?>">
        </div>
        <div class="form-group">
            <label for="status_peg">Status Pegawai</label>
            <select class="form-control" name="level">
                <option value="Honorer" <?= ($mem['status_peg']=="Honorer")? "selected" : "" ?>>Honorer</option>
                <option value="Tetap" <?= ($mem['status_peg']=="Tetap")? "selected" : "" ?>>Tetap</option>
            </select>
        </div>
        <div class="form-group">
            <label for="photos">Foto</label><br>
            <img src="<?= $mem['url'] ?>" width="80px" height="100px" />
            <input type="hidden" id="photos" name="photos" value="<?= $mem['photos'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>

        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="pegawai.php" class="btn btn-success">Batal</a>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
