<?php
    class database{
        public $host = "localhost",
        $uname = "root",
        $pass = "",
        $db = "kantor",
        $con,
        $path = "images/";
        

        public function __construct()
        {   
            $this->con = mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
            date_default_timezone_set('Asia/Jakarta');
        }

        public function tampildata(){
            $sql = "SELECT m.id_member,m.nama,m.telepon,m.level, concat('http://localhost/belvania/images/',photos) as url
                FROM member m";
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }


        public function insertdata($id_member,$nm,$telepon,$level,$imstr,$file){
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            $sql = "INSERT into member(id_member, nama, telepon, level, photos) values(
                '$id_member','$nama','$telepon', '$level','$file')";
            $result=mysqli_query($this->con,$sql);
            if($result){
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $sql = "delete from member where id_member='$id_member'";
                    mysqli_query($this->con,$sql);
                    $hasil['respon']="gagal";
                    return $hasil;   
                }else{
                    $hasil['respon']="sukses";
                    return $hasil;
                }
            }else{
                $hasil['respon']="gagal";
                return $hasil;
            }
        }

        public function editdata($id_member){         // fungsi mengambil data
            $sql = "SELECT p.kode,p.nama,p.jenkel,p.level,p.photos,concat('http://localhost/kantor/images/',photos) as url
                FROM pegawai p
                WHERE p.kode ='$kode'"; 
            $data = mysqli_query($this->con,$sql);
            while ($d = mysqli_fetch_array($data))
            {
                $hasil[] = $d;
            }
            return $hasil;
        }

        public function editdata($kode,$nama,$jenkel,$status,$imstr,$file){ // fungsi mengudapte data
            $nama=mysqli_real_escape_string($this->con,trim($nm));
            if($imstr==""){
                $sql = "UPDATE pegawai SET nama='$nama', jenkel='$jenkel', statuss='$statuss'
                where kode='$kode'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //update data sukses tanpa foto
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }else{
                if(move_uploaded_file($imstr,$this->path.$file) == false){
                    $hasil['respon']="gagal";
                    return $hasil;  
                }else{
                    $sql = "UPDATE member SET nama='$nama',telepon='$telepon',level='$level',photos='$file'
                            where id_member='$id_member'";
                    $result = mysqli_query($this->con,$sql);
                    if($result){
                        $hasil['respon']="sukses";
                        return $hasil; //update data sukses semua
                    }else{
                        $hasil['respon']="gagal";
                        return $hasil;
                    }
                }
            }
        }      
        public function deletedata($id_member){
            $sql = "SELECT photos from member where id_member='$id_member'";
            $result = mysqli_query($this->con,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photos = $data['photos'];
                    unlink($this->path.$photos);
                }
                $sql = "DELETE from member where id_member='$id_member'";
                $result = mysqli_query($this->con,$sql);
                if($result){
                    $hasil['respon']="sukses";
                    return $hasil; //delete data sukses
                }else{
                    $hasil['respon']="gagal";
                    return $hasil;
                }
            }
        }
    }
?>