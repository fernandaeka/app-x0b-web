<?php
    $DB_NAME = "kantor";
	$DB_USER = "root";
	$DB_PASS = "";
	$DB_SERVER_LOC = "localhost";
	$conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Pegawai - Tambah Data</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    <!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
        <a class="navbar-brand" href="index.php">Pegawai</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="index.php">Home</a>
                <a class="nav-item nav-link" href="about.php">Pegawai</a>
                <a class="nav-item nav-link active" href="member.php">Item<span class="sr-only">(current)</span></a>
            </div>
        </div>
        </div>
    </nav>
    <!-- Navbar End -->
<div class="container">
<h4 class="mt-3 mb-3">Tambah Data Pegawai</h4>
<form action="proses.php?aksi=insert" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="kode">Kode</label>
            <input type="text" placeholder="Masukkan ID Pegawai" id="kode" name="kode" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="nama">Nama Pegawai</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="jenkel">Jenis Kelamin</label>
            <input type="text" placeholder="Masukkan Jenis Kelamin" id="jenkel" name="jenkel" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="status_peg">Status Pegawai</label>
            <select class="form-control" name="status_peg">            
                <option value="Honorer">Honorer</option>          
                <option value="Tetap">Tetap</option>
            </select>
        </div>
        <div class="form-group">
            <label for="photos">Foto</label>
            <input type="file" class="form-control-file" id="file" name="file">
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a href="pegawai.php" class="btn btn-success">Batal</a>
        </div>
</form>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>
