<?php
    include 'koneksi.php';
    $db = new database();
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Pegawai</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-success">
        <a class="navbar-brand" href="index.php"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="pegawai.php">Pegawai</a>
                </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Item
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="status.php">Status</a>
                </div>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
<h4 class="mt-3 mb-3">Update Data Pegawai</h4>

<?php foreach($db->editdata($_GET['kode']) as $mem) : ?>
<form action="proses.php?aksi=update" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
        <div class="form-group">
            <label for="id_member">ID</label>
            <input type="hidden" id="kode" name="kode" value="<?= $mem['kode'] ?>"><br>
            <?= $mem['kode'] ?>
        </div>
        <div class="form-group">
            <label for="nama">Nama Pegawai</label>
            <input type="text" placeholder="Masukkan Nama" id="nama" name="nama" class="form-control" required value="<?= $mem['nama'] ?>">
        </div>
        <div class="form-group">
            <label for="telepon">Jenis Kelamin</label>
            <input type="text" placeholder="Masukkan Jenis Kelamin" id="jenkel" name="Masukkan Jenis Kelamin" class="form-control" required value="<?= $mem['jenkel'] ?>">
        </div>
        <div class="form-group">
            <label for="level">Status</label>
            <select class="form-control" name="level">
                <option value="Honorer"      <?= ($mem['status_peg']=="Honorer")? "selected" : "" ?>>Honorer</option>
                <option value="Tetap"        <?= ($mem['status_peg']=="Tetap")? "selected" : "" ?>>Tetap</option>
            </select>
        </div>
        <div class="form-group">
            <label for="photos">Foto</label><br>
            <img src="<?= $mem['url'] ?>" width="80px" height="100px" />
            <input type="hidden" id="photos" name="photos" value="<?= $mem['photos'] ?>">
            <input type="file" class="form-control-file" id="file" name="file">
        </div>

        <button type="submit" class="btn btn-dark">Simpan</button>
        <a href="pegawai.php" class="btn btn-dark">Batal</a>
        </div>
</form>
<?php endforeach ?>
</div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>